public class Main {

    public static void main(String[] args) {
        Student student1 = new Student();
        student1.name = "John Doe";
        student1.id = 1234;
        student1.major = "Computer Science";
        student1.year = 3;

        Student student2 = new Student();
        student2.name = "Jane Smith";
        student2.id = 5678;
        student2.major = "Biology";
        student2.year = 2;

        Course course1 = new Course();
        course1.name = "Introduction to Computer Science";
        course1.id = 1001;
        course1.credit = 4;

        Course course2 = new Course();
        course2.name = "Organic Chemistry";
        course2.id = 2001;
        course2.credit = 4;

        Course course3 = new Course();
        course3.name = "Data Structures and Algorithms";
        course3.id = 1002;
        course3.credit = 4;

        Transcript[] transcripts = new Transcript[3];

        Transcript t1 = new Transcript();
        t1.student = student1;
        t1.course = course1;
        t1.grade = 85;
        transcripts[0] = t1;

        Transcript t2 = new Transcript();
        t2.student = student1;
        t2.course = course3;
        t2.grade = 92;
        transcripts[1] = t2;

        Transcript t3 = new Transcript();
        t3.student = student2;
        t3.course = course2;
        t3.grade = 78;
        transcripts[2] = t3;

        Transcript t4 = new Transcript();
        t4.student = student1;
        t4.course = course2;
        t4.grade = 90;
        addTranscript(student1, t4, transcripts);

        printTranscripts(student1, transcripts);
        printTranscripts(student2, transcripts);
    }

    public static void addTranscript(Student student, Transcript transcript, Transcript[] transcripts) {
        Transcript[] newTranscripts = new Transcript[transcripts.length + 1];
        for (int i = 0; i < transcripts.length; i++) {
            newTranscripts[i] = transcripts[i];
        }
        newTranscripts[transcripts.length] = transcript;
        student.transcripts = newTranscripts;
    }

    public static void printTranscripts(Student student, Transcript[] transcripts) {
        System.out.println("Transcripts for " + student.name + ":");
        for (Transcript transcript : transcripts) {
            if (transcript.student == student) {
                printTranscript(transcript);
            }
        }
    }

    public static void printTranscript(Transcript transcript) {
        System.out.println(transcript.course.name + ": " + transcript.grade);
    }

    public static class Student {
        String name;
        int id;
        String major;
        int year;
        Transcript[] transcripts;
    }

    public static class Course {
        String name;
        int id;
        int credit;
    }

    public static class Transcript {
        Student student;
        Course course;
        int grade;
    }
}