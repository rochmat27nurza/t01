import javax.sound.sampled.SourceDataLine;

public class Main {
    public static class Mahasiswa {
        String nama;
        int nim;
        String prodi;
        int tahunmasuk;
        Transkrip[] transkrip;
    }

    public static class Kursus {
        String nama;
        int kodematkul;
        int sks;
    }

    public static class Transkrip {
        Mahasiswa mhs;
        Kursus kursus;
        int nilai;
    }

    public static void addTranscript(Mahasiswa mahasiswa, Transkrip transkrip, Transkrip[] transkrips) {
        Transkrip[] transkripBaru = new Transkrip[transkrips.length + 1];
        for (int i = 0; i < transkrips.length; i++) {
            transkripBaru[i] = transkrips[i];
        }
        transkripBaru[transkrips.length] = transkrip;
        mahasiswa.transkrip = transkripBaru;
    }

    public static void title(Mahasiswa mhs, Transkrip[] transkrips) {
        System.out.println("Transkrip nilai untuk " + mhs.nama + ":");
        for (Transkrip transcript : transkrips) {
            if (transcript != null && transcript.mhs == mhs) {
                cetakTranscript(transcript);
            }
        }
    }

    public static void cetakTranscript(Transkrip transcript) {
        System.out.println(transcript.kursus.nama + ": " + transcript.nilai);
    }

    public static void main(String[] args) {
        Mahasiswa mhs1 = new Mahasiswa();
        //LENGKAPI CODE DISINI (5)
        mhs1.nama       = "Nur Rochmat Zawawi";
        mhs1.nim        = 22543;
        mhs1.prodi      = "Teknologi Informasi";
        mhs1.tahunmasuk = 2022;

        Mahasiswa mhs2 = new Mahasiswa();
        //LENGKAPI CODE DISINI (2.5)
        mhs2.nama       = "Zidane El Fasya";
        mhs2.nim        = 22588;
        mhs2.prodi      = "Teknologi Informasi";
        mhs2.tahunmasuk = 2022;
        

        Kursus mk1 = new Kursus();
        //LENGKAPI CODE DISINI (5)
        mk1.nama        = "Pemograman Lanjut";
        mk1.kodematkul  = 1;
        mk1.sks         = 5;
       
        Kursus mk2 = new Kursus();
        //LENGKAPI CODE DISINI (2.5)
        mk2.nama        = "Pengantar Sistem Operasi";
        mk2.kodematkul  = 2;
        mk2.sks         = 3;
       
        Transkrip[] transcripts = new Transkrip[4];
        Transkrip t1 = new Transkrip();
        //LENGKAPI CODE DISINI (7.5)
        t1.mhs = mhs1;
        t1.kursus = mk1;
        t1.nilai = 98;
        transcripts[0] = t1;


        Transkrip t2 = new Transkrip();
        //LENGKAPI CODE DISINI (2.5)
        t2.mhs =mhs1;
        t2.kursus=mk2;
        t2.nilai= 88;
        transcripts[1] = t2;

        Transkrip t3 = new Transkrip();
        //LENGKAPI CODE DISINI (2.5)
        t3.mhs = mhs2;
        t3.kursus=mk1;
        t3.nilai = 85;
        transcripts[2]=t3;

        Transkrip t4 = new Transkrip();
        //LENGKAPI CODE DISINI (2.5)
        t4.mhs = mhs2;
        t4.kursus=mk2;
        t4.nilai=89;
        transcripts[3]=t4;
  
        addTranscript(mhs1, t1, transcripts); 
        addTranscript(/*LENGKAPI CODE DISINI (2)*/mhs1, t2, transcripts);
        title(/*LENGKAPI CODE DISINI (3)*/mhs1, transcripts);
        addTranscript(/*LENGKAPI CODE DISINI (5)*/mhs2, t3, transcripts);
        addTranscript(/*LENGKAPI CODE DISINI (2)*/mhs2, t4, transcripts);
        title(/*LENGKAPI CODE DISINI (3)*/mhs2, transcripts);
    }
}